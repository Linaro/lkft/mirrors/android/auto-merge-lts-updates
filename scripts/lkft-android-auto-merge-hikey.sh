#!/bin/bash -ex

# this script will be called by the tuxtrigger in the following format:
# subprocess.run([script_path, repo_url, branch, new_sha]

# Where the LTS updates to be fetched, normall it would be:
#   https://android.googlesource.com/kernel/common
source_url="${1}"
# The upstream branches to be tracked, which are:
#   android-4.14-stable / android-4.19-stable
source_branch="${2}"
# The new commit sha from the upstream branch
source_sha="${3}"

# Where the LTS updates will be merged into for the hikey repository
# and branches:
#   android-hikey-linaro-4.14-stable-lkft / android-hikey-linaro-4.19-stable-lkft
target_repo_url_hikey="https://${GITLAB_LKFT_BOT_ACCOUNT}:${GITLAB_LKFT_BOT_TOKEN}@gitlab.com/Linaro/lkft/mirrors/android/linaro-hikey.git"

function merge_lts(){
    local lts_repo_url="${1}"
    local lts_repo_branch="${2}"
    local lts_repo_sha="${3}"
    local linaro_repo_url="${4}"
    local linaro_repo_branch="${5}"

    local dir_working="auto-merge-workspace"
    local remote_name="linaro"

    git config --global user.name "Linaro Gitlab CI"
    git config --global user.email "lkft-bot@linaro.org"

    # remote android-4.9-q-hikeye name is origin
    rm -fr "${dir_working}"
    git clone -b "${lts_repo_branch}" "${lts_repo_url}" "${dir_working}"
    cd "${dir_working}"
    git remote add -t "${linaro_repo_branch}" "${remote_name}" "${linaro_repo_url}"
    git fetch --all
    git checkout -B "${linaro_repo_branch}" "remotes/${remote_name}/${linaro_repo_branch}"
    git merge --log --no-edit "${lts_repo_branch}"
    # push to the remote branch
    git push "${remote_name}" HEAD:"${linaro_repo_branch}"

    # Apply tag
    # TODO to get the upstream commit information
    kernel_version=$(make kernelversion)
    tag=${kernel_version}-${lts_repo_sha:0:12}-$(date +"%Y%m%d")
    git tag "${tag}"

    # Push tag
   git push "${remote_name}" "${tag}"
}

case "X${source_branch}" in
    "Xandroid-4.19-stable")
        target_branch="android-hikey-linaro-4.19-stable-lkft"
        target_url="${target_repo_url_hikey}"
        merge_lts "${source_url}" "${source_branch}" "${source_sha}" \
                    "${target_url}" "${target_branch}"
        ;;
    "X*")
        echo "${source_branch} is not supported yet"
        ;;
esac
