#!/bin/bash -ex

if [ -z "${TUXTRIGGER_CONFIG}" ]; then
    echo "No tuxtrigger configuration file is specified"
    exit 0
fi

dir_out="out"
# the directory of this repository
dir_workspace="${PWD}"
mkdir -p "${dir_out}"
tuxtrigger "${TUXTRIGGER_CONFIG}" \
        --disable-plan \
        --pre-submit "${dir_workspace}/scripts/lkft-android-auto-merge-hikey.sh" \
        --plan "${dir_workspace}" \
        --submit=change \
        --output "${dir_out}/output_file.yaml" \
        --log-level=INFO --log-file "${dir_out}/log.txt"
