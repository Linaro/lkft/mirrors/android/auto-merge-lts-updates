#!/bin/bash -ex

if [ -n "${LTSK_IGNORE_COMMIT_SHA}" ]; then
    echo "Ignore the check for the previous commit sha, trigger a new build anyway"
    exit 0
fi

# Install curl and jq for API requests and JSON parsing
apt-get update && apt-get install -y curl unzip

curl_array=("curl")
curl_array+=("-fsSL" "${CI_PROJECT_URL}/-/jobs/artifacts/main/download?job=${CI_JOB_NAME}")
curl_array+=("--output" "artifacts.zip")

if "${curl_array[@]}"; then
    unzip -o artifacts.zip out/output_file.yaml
else
    echo "Failed to download previous artifacts.zip to check the commit SHA information"
    echo "Merge or mirror will be triggered anyway"
fi
